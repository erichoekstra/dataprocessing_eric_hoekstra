import os

rule all:
    """ Final rule """
    input: "output/figures/"

rule read_sequence:
    """ Reading protein sequence and printing it """
    input: 'data/'
    output: 'output/all_sequences.fasta'
    run:
        import glob
        input_folder = "data"
        # Finding all fasta files
        file_list = glob.glob(input_folder + "/*.fasta")

        sequence_list = []
        # opening output file inwrite
        output_file = open(output[0], 'w')

        # Extracting sequences and putting it in a single file
        for file in file_list:
            opened = open(file)
            for line in opened:
                if not line.startswith(">"):
                    output_file.write(line)

        output_file.close()

rule split_string_into_list:
    """Splits the strings into lists and writes this list to a .fasta file"""
    input: "output/all_sequences.fasta"
    output: "output/output_ssil_sequences.fasta"
    run:
        file = open(input[0])
        # Splitting the string into list and writing these to a fasta file
        with open(output[0], 'w') as sequences:
            sequence_list = [line.strip() if not line == "\n" else line for line in file]
            sequences.write(str(sequence_list))

rule remove_first_character:
    """ Removing first character of each string"""
    input: "output/output_ssil_sequences.fasta"
    output: "output/output_rfc_sequences.fasta"
    run:
        # Making list with each separate sequence without the enter
        string_list = ""
        with open(output[0], 'w') as output_file:
            with open(input[0], 'r') as input:
                for line in input:
                    string_list += line

                # Creates the list of the string representation
                sequence_list = "".join(eval(string_list))
                sequences = sequence_list.split("\n")

                # writes the sequences
                [output_file.write(line[1:] + "\n") for line in sequences]

rule searchSimple:
    """ Searches in BLAST database with sequences"""
    input: "output/output_rfc_sequences.fasta"
    output: directory("output/blast_results/")
    run:
        import Bio.Blast.NCBIWWW

        counter = 0
        file = open(input[0], 'r')
        # create output directory
        os.mkdir(output[0])

        # Perform blast per sequence
        for sequence in file:
            counter += 1
            if sequence.strip():
                with open(output[0] + "/blast_result" + "_" + str(counter) + ".xml", "w") as save_to:
                    result_handle = Bio.Blast.NCBIWWW.qblast("blastp", "nr", sequence)
                    save_to.write(result_handle.read())
                    result_handle.close()
                    print("Blast search done, moving to the next one if there's a next one...")

rule visualise_blast:
    """ Visualises the protein BLAST result, it makes a graph of all the results and plots the identity"""
    input: "output/blast_results/"
    output: directory("output/figures/")
    run:
        from parse_XML import parseXML
        from plotting_blast_result import plot_blast
        import glob

        # Finds the blast results in the output/blast_results folder
        blast_results = glob.glob(input[0] + "/*.xml")

        # Performs the XML parsing and blast result parsing per .xml file
        for index, i in enumerate(blast_results):
            stats_dict = parseXML(i)

            # create output directory for blast results
            os.system("mkdir " + output[0])

            plot_blast(stats_dict, index)

