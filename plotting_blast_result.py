#!/usr/bin/env python3

"""
Small script to parse BLAST xml script

This script is a standalone script which is not runnable via the command line.
To use this import the function plot_blast into a desired script.
"""

__author__ = "Eric hoekstra"
__version__ = 1.0


import matplotlib
from matplotlib import pyplot as plt

def plot_blast(stats, index):
    # These are the stats from the hits that need to be plotted
    desired_outputs = ["Hsp_bit-score", "Hsp_score", "Hsp_identity"]
    print(stats)

    # For each value in the dictionairy check if it is in the desired outputs.
    for i in desired_outputs:
        matplotlib.use('agg')

        # Plotting the figure
        plt.figure()
        plt.plot(stats[i])
        plt.title(i)
        plt.gca().invert_yaxis()
        plt.xlabel("Hit index")
        plt.ylabel("Amount")
        # Saves the figure to output file
        plt.savefig("output/figures/some_figure_" + i + "_" + str(index) + ".png")
