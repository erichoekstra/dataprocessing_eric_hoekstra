#!/usr/bin/env python3

"""
Small script to plot blast results
"""

__author__ = "Eric hoekstra"
__version__ = 1.0

import xml.etree.ElementTree as ET


def parseXML(i):
    tree = ET.parse(i)
    root = tree.getroot()
    # manier vinden om bij de hit root te komen en dan die resultaten te interpreteren
    root.iter()

    stats_dict = {}

    # Logic to parse XML and save the desired information into a dictionary
    for hit in root.iter("Hit"):
        for child in hit.iter("Hit_hsps"):
            for kid in child.iter():
                # Checks if it was already added to dict
                if kid.tag not in stats_dict:
                    # Adds values to dict
                    stats_dict[kid.tag] = []
                    stats_dict[kid.tag].append(kid.text)
                else:
                    stats_dict[kid.tag].append(kid.text)

    return stats_dict
