**Thema 11 - Dataprocessing Assessment - Eric Hoekstra - Version 1.0**

This repo  contains the contents of the Dataprocessing assessment of theme 11.

It contains a pipeline written in Snakemake to parse multiple .fasta files and perform BLAST on them.

![](dag.svg)

There are 6 rules, most of them are self-explanatory. The rule searchSimple will perform BLASTp on protein sequences per sequence given. Visualise blast will then visualise the results of this the BLAST by parsing the .xml output. 

---

## Dependencies
 
These are the dependencies, the biopython library can be installed by configuring an interpreter and installing the library Bio.

Dependencies:  
- Snakemake (version 5.32.2)  
- Biopython (version 1.78)  
- Python (version 3.9, should also work in 3.8)  
- Some standard Python libraries which are incorporated in Python  

## Example input data
The tool will expect a folder, called 'data', with .fasta files in it in this format:

```
>QNR60016.1 ORF8 protein [Severe acute respiratory syndrome coronavirus 2]
MKFPVFLGIITTVAAFQQECSLQSCTQHQPYVVDDPCPIHFYSKWYIRVGARKSAPLIELCVDEAGSKSP
IQYIDIGNYTVSCLPFTINCQEPKLGSLVVRCSFYEDFFEYHDVRVVLDFIMKFPVFLGIITTVAAFQQECSLQSCTQHQPYVVDDPCPIHFYSKWYIRVGARKSAPLIELCVDEAGSKSP
IQYIDIGNYTVSCLPFTINCQEPKLGSLVVRCSFYEDFFEYHDVRVVLDFIMKFPVFLGIITTVAAFQQECSLQSCTQHQPYVVDDPCPIHFYSKWYIRVGARKSAPLIELCVDEAGSKSP
IQYIDIGNYTVSCLPFTINCQEPKLGSLVVRCSFYEDFFEYHDVRVVLDFI
MKFPVFLGIITTVAAFQQECSLQSCTQHQPYVVDDPCPIHFYSKWYIRVGARKSAPLIELCVDEAGSKSP
IQYIDIGNYTVSCLPFTINCQEPKLGSLVVRCSFYEDFFEYHDVRVVLDFI
MKFPVFLGIITTVAAFQQECSLQSCTQHQPYVVDDPCPIHFYSKWYIRVGARKSAPLIELCVDEAGSKSP
IQYIDIGNYTVSCLPFTINCQEPKLGSLVVRCSFYEDFFEYHDVRVVLDFI
```

Next to that, nucleotide files are NOT supported. Only protein sequences are supported.

## Set-Up

1. Clone the repo via the command line onto your own pc
2. Put your .fasta files in the 'data' folder
3. Make sure you have snakemake installed
4. Navigate to output file and run snakemake:
 
```bash
~ dataprocessing $ snakemake snakemake --cores all
```
Every PC is different but it should work like this. If it's not able to find the snakefile then use -S to specify the snakefile location.

---

## The output

For every .fasta file an BLAST .xml file will be created. This one is located in the output/blast_results folder. They are ordered the same way as the .fasta files are ordered in the data folder. 

Next to that, the .xml BLAST result will be visualised using 3 different plots. They are also ordered the same way as the .xml and .fasta files. That way you can distinguish between them.

---

## Contact

For contact: e.j.hoekstra@st.hanze.nl